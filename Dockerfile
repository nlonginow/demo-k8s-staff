FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/staff-0.0.1-SNAPSHOT.jar app.jar
ENV JAVA_OPTS="-Dserver.port=8082"
ENTRYPOINT exec java -jar -Dserver.port=8082 /app.jar 
